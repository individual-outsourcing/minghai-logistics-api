const Base = require('./base.js');
// 用户登录状态下所有控制器都应该要继承此
module.exports = class extends Base {
    /**
     * 判断用户是否登录，所有需要登录的页面继承此类
     */
    async __before() {
        var _this = this;
        const flag = await super.__before();
        // 如果父级想阻止后续继承执行会返回 false，这里判断为 false 的话不再继续执行了。
        if (flag === false) return false;
        // 其他逻辑代码
        if (!_this.header.hasOwnProperty('openid') || !_this.header['openid']) {
            return _this.fail(1, '当前微信账户尚未完成绑定');
        }
        try {
            _this.user = await think.model('wx_binduser','mh_user').where({"OpenId":this.header['openid']}).find();
        } catch (err) {
            return _this.fail(1, '用户信息异常,请联系管理员');
        }
    }

    /**
    * 获取油卡充值记录表
    * 员工可以看所有  司机只能看到自己相关的
    */
    async getOilfillistAction(){
    	let sType = this.user.sType;
    	let list =[];
    	if(sType==1)
    	{
    		list = await think.model('list_oilcard','mh_oilcard').select();
    	}else if(sType==3)
    	{
    		let suplierId = this.user.SupplierId;
    		let number = await think.model('trucker','mh_supplier').alias('t')
    		.join(['inner join mh_supplier.truck tk on tk.Id = t.truckId'])
    		.where({'t.Id':suplierId}).field('tk.number').find();
    		list = await think.model('list_oilcard','mh_oilcard').where({'OwnerTruck':number.number}).select();
    	}
    	return this.success(list,'ok');
    }

    /**
    * 获取油卡的消费记录
    */
    async getOildetailAction(){
    	let card = this.get('card');
    	let list = await think.model('truck_oilfilup','mh_finance').where({'CardNumber':card}).select();
    	return this.success(list,'ok');
    }
}