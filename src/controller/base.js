module.exports = class extends think.Controller {
    async __before() {
        this.header = this.ctx.headers;
    }

    __call() {
        return this.fail(1, '不存在的api');
    }

    /**
     * 字典排序
     * @param {*} obj 
     */
    sortObjectKeys(obj) {
        var tmp = {};
        Object.keys(obj).sort().forEach(function (k) {
            try {
                tmp[k] = JSON.parse(obj[k]);
            } catch (e) {
                tmp[k] = obj[k];
            }
        });
        return tmp;
    }
    /**
     * 对象转url参数
     * @param {*} param
     * @param {*} key
     * @param {*} encode
     */
    urlEncode(param, key, encode) {
        if (param == null) return '';
        var paramStr = '';
        var t = typeof param;
        if (t === 'string' || t === 'number' || t === 'boolean') {
            paramStr += '&' + key + '=' + (encode == null || encode ? encodeURIComponent(param) : param);
        } else {
            for (var i in param) {
                var k = key == null ? i : key + (param instanceof Array ? '[' + i + ']' : '.' + i);
                paramStr += this.urlEncode(param[i], k, encode);
            }
        }
        return paramStr;
    }

};
//# sourceMappingURL=base.js.map