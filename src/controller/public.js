const Base = require('./base.js');
// 无需验证签名的控制器
module.exports = class extends Base {
    async __before(){
        const flag = await super.__before();
        // 如果父级想阻止后续继承执行会返回 false，这里判断为 false 的话不再继续执行了。
        if (flag === false) return false;
    }

    /**
    * 绑定账户 无用户信息时 免去授权步骤
    */
    async binduserAction(){
        let name = this.post('name');
        let password = this.post('password');
        let openid = this.post('openid');
        let unionid = this.post('unionid');
        if(think.isEmpty(name))
        {
            return this.fail(1,'请填写账户');
        }
        if(think.isEmpty(password))
        {
            return this.fail(1,'请填写密码');
        }
        if(think.isEmpty(openid))
        {
            return this.fail(1,'请完成微信授权');
        }
        let user = await think.model('user','mh_user').where({'name':name}).find();
        if(think.isEmpty(user))
        {
           return this.fail(1,'不存在的用户'); 
        }
        let decodepwd = global.decryptPwd(user.password);
        if(decodepwd!=password)
        {
            return this.fail(1,'密码错误');
        }
        let bduser = await think.model('wx_binduser','mh_user').where({"OpenId":openid}).find();
        if(!think.isEmpty(bduser))
        {
            return this.fail(1,'您已经完成过绑定了');
        }


        let thecname ='';
        if(user.sType==3)
        {

            let bdtrucker = await think.model('wx_binduser','mh_user').where({"userid":user.Id}).find();
            if(!think.isEmpty(bdtrucker))
            {
                return this.fail(1,'该用户已完成过绑定了');
            }


            let theCName = await think.model('trucker','mh_supplier').where({'Id':user.SupplierId}).field('name').find();
            if(!think.isEmpty(theCName))
            {
                thecname=theCName.name;
            }

        }
        //开始绑定
        let map ={
            "sDate":global.date(parseInt(Date.now() / 1000),1),
            "UseName":user.name,
            "OpenId":openid,
            "sType":user.sType,
            "SupplierId":user.SupplierId,
            "wxName":'',
            "country":'',
            "Area":'',
            "city":'',
            "CName":thecname,
            "userid":user.Id,
            "unionid":unionid||""
        }
        let result = await think.model('wx_binduser','mh_user').add(map);
        if(!think.isEmpty(result))
        {
            let newuser = {
                "username":user.name,
                "nameOfEnglish":user.nameOfEnglish,
                "brithday":user.brithday,
                "mobile":user.mobile,
                "sType":user.sType,
                "openid":openid
            };
            return this.success(newuser,'绑定成功');
        }else{
            return this.fail(1,'绑定失败')
        }
    }

    /**
    * 绑定账户 授权后的绑定
    */
    async loginAction (){
        let name = this.post('name');
        let password = this.post('password');
        let openid = this.post('openid');
        let userinfo = this.post('userinfo');
        if(think.isEmpty(name))
        {
            return this.fail(1,'请填写账户');
        }
        if(think.isEmpty(password))
        {
            return this.fail(1,'请填写密码');
        }
        if(think.isEmpty(openid))
        {
            return this.fail(1,'请等待微信授权完成');
        }
        if(!think.isEmpty(userinfo))
        {
            userinfo=JSON.parse(userinfo);
        }
        
        let user = await think.model('user','mh_user').where({'name':name}).find();
        if(think.isEmpty(user))
        {
           return this.fail(1,'不存在的用户'); 
        }
        let decodepwd = global.decryptPwd(user.password);
        if(decodepwd!=password)
        {
            return this.fail(1,'密码错误');
        }
        let bduser = await think.model('wx_binduser','mh_user').where({"OpenId":openid}).find();
        if(!think.isEmpty(bduser))
        {
            return this.fail(1,'您已经完成过绑定了');
        }



        let thecname ='';
        if(user.sType==3)
        {

            let bdtrucker = await think.model('wx_binduser','mh_user').where({"userid":user.Id}).find();
            if(!think.isEmpty(bdtrucker))
            {
                return this.fail(1,'该用户已完成过绑定了');
            }

            let theCName = await think.model('trucker','mh_supplier').where({'Id':user.SupplierId}).field('name').find();
            if(!think.isEmpty(theCName))
            {
                thecname=theCName.name;
            }
        }
        //开始绑定
        let map ={
            "sDate":global.date(parseInt(Date.now() / 1000),1),
            "UseName":user.name,
            "OpenId":openid,
            "sType":user.sType,
            "SupplierId":user.SupplierId,
            "wxName":global.filterNicknameWithEmoj(userinfo.nickname),
            "country":userinfo.country,
            "Area":userinfo.province,
            "city":userinfo.city,
            "CName":thecname,
            "userid":user.Id,
            "unionid":userinfo.unionid||""
        }
        let result = await think.model('wx_binduser','mh_user').add(map);
        if(!think.isEmpty(result))
        {
            let newuser = {
                "username":user.name,
                "nameOfEnglish":user.nameOfEnglish,
                "brithday":user.brithday,
                "mobile":user.mobile,
                "sType":user.sType,
                "openid":openid
            };
            return this.success(newuser,'绑定成功');
        }else{
            return this.fail(1,'绑定失败')
        }
    }

    /*
    * 根据code获取微信用户的信息 并判断是否已完成账户绑定
    */
    async getWxinfoAction(){
        let code = this.post('code');
        if(think.isEmpty(code))
        {
            return this.fail(1,'参数异常,获取授权信息失败');
        }
        let res = await think.service('wxUtil').getopenid(code);
        if(res==false)
        {
            return this.fail(1,'获取授权信息失败');
        }else{
            let openid = res.openid;
            let bindinfo = await think.model('wx_binduser','mh_user').where({"OpenId":openid}).find();
            if(!think.isEmpty(bindinfo))
            {
                let user = await think.model('user','mh_user').where({'Id':bindinfo.userid}).field('Id as id,name as username,nameOfEnglish,brithday,mobile,sType').find();
                let newuser={
                    "openid":openid,
                    "id":bindinfo.userid,
                    "username":bindinfo.UseName,
                    "nameOfEnglish":user.nameOfEnglish,
                    "brithday":user.brithday,
                    "mobile":user.mobile,
                    "sType":bindinfo.sType,
                    "unionid":res.unionid||""
                }
                return this.success({status:1,data:newuser},'您已完成过绑定了');
            }else{
                return this.success({status:0,data:res},'授权成功');
            }
        }
    }

    /**
    * 根据小程序code获取openid等信息
    */
    async getAppOpenidAction(){
        let code = this.post('code');
        if(think.isEmpty(code))
        {
            return this.fail(1,'参数异常,获取授权信息失败');
        }
        let res = await think.service('wxUtil').code2Session(code);
        if(res==false)
        {
            return this.fail(1,'获取授权信息失败');
        }else{
            let openid = res.openid;
            let unionid = res.unionid;
            if(think.isEmpty(openid))
            {
                return this.fail(1,'获取授权信息失败');
            }
            if(think.isEmpty(unionid))
            {
                return this.fail(1,'当前用户尚未关注明海物流公众号,请关注后再来使用');
            }
            let bindinfo = await think.model('wx_binduser','mh_user').where({"unionid":unionid}).find();
            if(!think.isEmpty(bindinfo))
            {
                let user = await think.model('user','mh_user').where({'Id':bindinfo.userid}).field('Id as id,name as username,nameOfEnglish,brithday,mobile,sType').find();
                let newuser={
                    "openid":bindinfo.OpenId,
                    "id":bindinfo.userid,
                    "username":bindinfo.UseName,
                    "nameOfEnglish":user.nameOfEnglish,
                    "brithday":user.brithday,
                    "mobile":user.mobile,
                    "sType":bindinfo.sType,
                    "city":bindinfo.city,
                    "sFrom":user.sFrom
                }
                return this.success(newuser,'授权登录成功');
            }else{
                // return this.success({status:0,data:res},'授权成功');
                return this.fail(1,'您尚未进行绑定,请前往明海物流公众号进行账户绑定');
            }
        }
    }

    /**
    *获取区域列表
    */
    async getAreaListAction(){
        let list = await think.model('truck_pricelist','mh_truck').where({"isdelete":0}).field('area').group('area').order('area desc').select();
        return this.success(list,'ok');
    }

    /*
    * 获取运费信息
    * area 区域
    */
    async getPriceListAction(){
        let area = this.post('area');
        if(think.isEmpty(area))
        {
            return this.fail(1,'请选择区域');
        }
        let order ={
            "area":'asc',
            'place':'asc'
        };
        let list2 = await think.model('truck_pricelist','mh_truck').where({"isdelete":0,"area":area}).field('Id,area,place,sk_freight,yict_freight').group('place').order(order).select();
        return this.success(list2,'ok');
    }
    /**
    * 获取腾讯cos临时密钥
    */
    async getCosCredentialAction(){
        var res = await think.service('qcloud_cos').getCredential();
        if(!think.isEmpty(res))
        {
            if(res.status==true)
            {
                return this.success(res.data,'ok');
            }else{
                return this.fail(1,JSON.stringify(res.data));
            }
        }
        return this.fail(1,'获取临时密钥失败');
    }
}