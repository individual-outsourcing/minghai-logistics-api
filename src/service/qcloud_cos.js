var STS = require('qcloud-cos-sts');

// 配置参数
var config = {
    secretId: "AKIDd6Vr79zQfOjobCf5OGrDSCfw2YeYOlY0",   // 固定密钥
    secretKey: "iIrdOuZzXrynRkqrBMh3Em451wRq0vJV",  // 固定密钥
    proxy: '',
    durationSeconds: 3600,  // 密钥有效期
    // 放行判断相关参数
    bucket: 'minghai-1252942393', // 换成你的 bucket
    region: 'ap-shanghai', // 换成 bucket 所在地区
    allowPrefix: '*'
};

module.exports = class extends think.Service {
	async getCredential(){
		var scope = [{
	        action: [
                // 简单上传
                'name/cos:PutObject',
                'name/cos:PostObject',
                'name/cos:DeleteObject'
            ],
	        bucket: config.bucket,
	        region: config.region,
	        prefix: config.allowPrefix,
	    }];
	    var policy = STS.getPolicy(scope);
	    const rest =await new Promise(function(resolve,reject){
	    	STS.getCredential({
		        secretId: config.secretId,
		        secretKey: config.secretKey,
		        policy: policy,
		        durationSeconds: config.durationSeconds,
		    }, function (err, credential) {
		    	if(err==null)
		    	{
		    		resolve(credential);
		    	}else{
		    		reject(err);
		    	}
		        
		    });
	    }).then(function(res){
	    	return {status:true,data:res};
	    }).catch(function(err){
	    	return {status:false,data:err};
	    })
	    return rest;
	    
	}
}