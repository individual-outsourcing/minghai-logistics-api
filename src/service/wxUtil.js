var request = require('request');

const appId = 'wx59bc2356eefcc028'
const appSecret = '7effc6553ff74b67c3caeeac453c0336'

const smappId = 'wxe043c9b63a83ec06'
const smappSecret = '395604f14ea9d8f645fb1dee4fc0b195'
module.exports = class extends think.Service {


    //http请求
    async getopenid(code){
        let url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='+think.config('wx_appid')+'&secret='+think.config('wx_appsecret')+'&code='+code+'&grant_type=authorization_code';
        const rest =await new Promise(function(resolve){
            request({
                url: url,
                method:"GET",
                json: true,
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                }
            },function(error, response, body) {
                if(response.statusCode == 200){
                    let data = body;
                    let openid = data.openid;
                    let access_token = data.access_token;
                    resolve({openid:openid,access_token:access_token});
                }else{
                    reject(false);
                }
            })
        }).then(function(res){
            return res;
        }).catch(function(err){
            return false;
        });
        if(rest==false)
        {
            return false;
        }else{
            let res = await this.getWxuserinfo(rest.access_token,rest.openid);
            return res;
        }
    }
    async getWxuserinfo(access_token,openid){
        let url = 'https://api.weixin.qq.com/sns/userinfo?access_token='+access_token+'&openid='+openid+'&lang=zh_CN';
        const rest =await new Promise(function(resolve){
            request({
                url: url,
                method:"GET",
                json: true,
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                }
            },function(error, response, body) {
                if(response.statusCode == 200){
                    resolve(body);
                }else{
                    reject(false);
                }
            })
        }).then(function(res){
            return res;
        }).catch(function(err){
            return false;
        });
        return rest;
    }

    async code2Session(code) {
        let url = 'https://api.weixin.qq.com/sns/jscode2session?appid='+smappId+'&secret='+smappSecret+'&js_code='+code+'&grant_type=authorization_code';
        const rest =await new Promise(function(resolve){
            request({
                url: url,
                method:"GET",
                json: true,
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                }
            },function(error, response, body) {
                if(response.statusCode == 200){
                    let data = body;
                    let openid = data.openid;
                    let session_key = data.session_key;
                    let unionid = data.unionid||null;
                    resolve({openid:openid,session_key:session_key,unionid:unionid});
                }else{
                    reject(false);
                }
            })
        }).then(function(res){
            return res;
        }).catch(function(err){
            return false;
        });
        return rest;
    }
};
