const view = require('think-view');
const model = require('think-model');
const cache = require('think-cache');
const fetch = require('think-fetch');
const session = require('think-session');

module.exports = [view, // make application support view
model(think.app), cache, fetch, session];
